# Démarrage et initialisation de serveur OpenVPN dans un docker

## Définir un nom de futur vpn
OVPN_NAME="network.vpn"
## Cloner le depot GIT 
Cloner avec avec `git clone git@gitlab.com:strategic.zone/docker-ovpn.git ${OVPN_NAME}`

`cd ${OVPN_NAME}`

## Définir le dossier DATA de futur conteneur vpn
OVPN_DATA="${PWD}/ovpn-data-${OVPN_NAME}"



## Création et initiation de server

> Remplacer le nom de container afin de correspondre au nom voulu de serveur vpn

`docker-compose run --rm nom_de_serveur_vpn ovpn_genconfig -u protocol://nom_de_serveur_vpn`

`docker-compose run --rm nom_de_serveur_vpn ovpn_initpki`

## Autres options

- -u SERVER_PUBLIC_URL
- -e EXTRA_SERVER_CONFIG
- -E EXTRA_CLIENT_CONFIG
- -f FRAGMENT
- -n DNS_SERVER ...
- -p PUSH ...
- -r ROUTE ...
- -s SERVER_SUBNET

### optional arguments:

- -2    Enable two factor authentication using Google Authenticator.
- -a    Authenticate  packets with HMAC using the given message digest algorithm (auth).
- -b    Disable 'push block-outside-dns'
- -c    Enable client-to-client option
- -C    A list of allowable TLS ciphers delimited by a colon (cipher).
- -d    Disable default route
- -D    Do not push dns servers
- -k    Set keepalive. Default: '10 60'
- -m    Set client MTU
- -N    Configure NAT to access external server network
- -t    Use TAP device (instead of TUN device)
- -T    Encrypt packets with the given cipher algorithm instead of the default one (tls-cipher).
- -z    Enable comp-lzo compression.	

## Sécurité
### Utilise au minimum la version 1.2 de TLS (qui est la seule version de TLS réellement sécurisé pour l'instant)
tls-version-min 1.2
-e "tls-version-min 1.2"
### Utilise l'ECDHE (Elliptic curve Diffie–Hellman) pour l'échange des clés + RSA pour l'authentification + AES-256-GCM-SHA384 (AES authentifié par Galois/Counter Mode avec SHA384) pour la poignée de main
tls-cipher TLS-ECDHE-RSA-WITH-AES-256-GCM-SHA384
`-e "tls-cipher TLS-ECDHE-RSA-WITH-AES-256-GCM-SHA384"`
### Utilise l'AES-256-CBC (Cipher Block Chaining) pour le chiffrement des données
cipher AES-256-CBC
`-e "cipher AES-256-CBC"`
### Utilise SHA512 pour l'authentification du chiffrement
auth SHA512
`-e "auth SHA512"`

### Regénère les clés de chiffrement toutes les 60 secondes
reneg-sec 60
`-e "reneg-sec 60"`
## Network topology
### Topology net30
topology net30
`-e "topology net30"
### Topology subnet
topology subnet
`-e "topology subnet"`

## Create client and export to single file

### with a passphrase (recommended)

`docker-compose run --rm ovpn_strat.zone easyrsa build-client-full client_name`

### without a passphrase (not recommended)

`docker-compose run --rm ovpn_strat.zone easyrsa build-client-full client_name nopass`

### Retrieve the client configuration with embedded certificates

`docker-compose run --rm vpn_server_name ovpn_getclient client_name > client_name.ovpn`

## Revoke client

### Keep the corresponding crt, key and req files.

`docker-compose run --rm ovpn_strat.zone ovpn_revokeclient client_name
### Remove the corresponding crt, key and req files.

`docker-compose run --rm ovpn_strat.zone ovpn_revokeclient client_name remove`
