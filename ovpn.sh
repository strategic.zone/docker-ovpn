#!/usr/bin/env bash
#title		:ovpn-tool.sh
#description	:
#author		:Valeriu Stinca
#email		:ts@strategic.zone
#date		:20180525
#version	:0.2
#notes		:
#===================
# set -e

if [ -f .env ]
then
	source .env
else
	echo "Please create and edit .env file !!"
	exit 1
fi

function gen_address {
	if ! host -t A "${OVPN_DOMAIN}" ; then
		curl "https://zonomi.com/app/dns/dyndns.jsp?host=${OVPN_DOMAIN}&api_key=${ZONOMI_APIKEY}"
	fi
}

function create_server {
	docker-compose run --rm ovpn ovpn_genconfig \
		-u "${PROTOCOL}"://"${OVPN_DOMAIN}":"${PORT}" \
		-s "${IP_RANGE}" \
		-r "${ROUTE}" \
		-n "${DNS1}" \
		-n "${DNS2}" \
		${EXTRA} \
		-C "${CIPHER}" \
		-m "${MTU}" \
		-e "tls-version-min ${TLS_VERSION}" \
		-e "topology ${TOPOLOGY}" \
		-p "route ${ROUTE}"
	docker-compose run --rm ovpn ovpn_initpki
}

function create_client {
	mkdir -p clients
	if [ "${2}" = "nopass" ]
	then
		if docker-compose run --rm ovpn easyrsa build-client-full "${1}" nopass; then
			docker-compose run --rm ovpn ovpn_getclient "${1}" > clients/"${1}-${OVPN_DOMAIN}.ovpn"
		fi
	else
		if docker-compose run --rm ovpn easyrsa build-client-full "${1}"; then
			docker-compose run --rm ovpn ovpn_getclient "${1}" > clients/"${1}-${OVPN_DOMAIN}.ovpn"
		fi
	fi
}

function otp_client {
	docker-compose run --rm ovpn ovpn_otp_user "${1}"
# 	echo "otpauth://totp/valerius@vpn.zeus.strat.zone?secret=MODAIYXG2NSZK2P7T2HTSJ4TJI" | qrencode -t utf8
}
function generate_all {
	# Generate all clients
	docker-compose run --rm ovpn ovpn_getclient_all
}

function revoke_client {
	# revoke
	# docker-compose run --rm ovpn ovpn_revokeclient "${1}"
	# revoke and remove
	docker-compose run --rm ovpn ovpn_revokeclient "${1}" remove
}

function list_clients {
	docker-compose run --rm ovpn ovpn_listclients
}

function show_stats {
	cat ./openvpn-status.log
}

if [ $# -lt 1 ]
then
        echo "Usage : $0 create-server | create-client client-name (nopass)| otp client-name | revoke-client client-name | list-clients | generate-clients"
        exit 1
fi

case $1 in
	create-server )
		create_server ;;
	create-client )
		create_client "${2}" ;;
	otp )
		otp_client "${2}" ;;
	revoke-client )
		revoke_client "${2}" ;;
	list-clients )
		list_clients ;;
	generate-clients )
		generate_all;;
	gen-address )
		gen_address;;
    stats )
        show_stats;;
	* )
		echo "Usage : $0 create-server / create-user client-name / revoke-client client-name / list-clients"
esac
